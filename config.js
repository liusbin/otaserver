module.exports = {
    port:4001,
    API:{
        GET_APK: '/api/downloadapk',
        UPLOAD_APK: '/api/uploadapk',
        CHECK_VER: '/api/check_version/'
    },
    APK_FOLDER: __dirname + '/files/apk/'
}