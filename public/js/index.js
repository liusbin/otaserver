
var config = require('../../config');
var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');
var multer = require('multer');

var cors = require('cors');
var request = require('request');
var filePath = __dirname +'/../binary/';
var myip = require('quick-local-ip');//https://stackoverflow.com/questions/10750303/how-can-i-get-the-local-ip-address-in-node-js

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'files/apk/');    // The apk folder. You have create this by your own.
    },
    filename: function (req, file, cb) {

        cb(null, file.originalname);
    }
});
var upload = multer({storage:storage});
/* ============================================================================
 * OTA server APIs
 * ===========================================================================*/

/* GET home page. */
global.appRoot = path.resolve(__dirname);

router.get('/',cors(), function(req, res, next) {
  // res.render('index', { title: 'Express', });
  //   res.status(config.status.OK).send('AWIND Key Server: ');

    res.render('../views/pages/index');
    //res.send('OTA Server');
});


router.post(config.API.CHECK_VER,cors(),function (req, res, next) {

    var ip = myip.getLocalIP4();
    console.log("ip = " + ip);
    var downloadLink = "";
    var apkName = "";
    fs.readdir(config.APK_FOLDER,function(err,files){
        files.forEach(function (file) {

            apkName = file;
            downloadLink = "http://"+ip+":"+config.port+config.API.GET_APK+"/"+apkName;
            console.log(downloadLink);
        });

        var startPos = apkName.indexOf("_",0);
        var endPos = apkName.indexOf(".apk",startPos);
        var versionNumber = apkName.substring(startPos+1,endPos).trim();
        var response = {status:"ok",version:versionNumber,url:downloadLink};
        res.send(response);
    });


});

router.get(config.API.GET_APK+'/:apkName',cors(), function(req, res, next) {

    var apkName = req.params.apkName;
    res.setHeader('Content-disposition', 'attachment; filename='+apkName);
    res.setHeader('Content-type', 'application/vnd.android.package-archive');

    res.download(config.APK_FOLDER+apkName,function(err){
        if (err) {
            console.log(err);
        } else {
            // decrement a download credit, etc.
        }
    });

});

router.post(config.API.UPLOAD_APK,cors(),upload.single('file'),function (req, res, next) {


    //console.log('body = ' + req.body);

    var file = req.file;
    console.log('file type：%s', file.mimetype);
    console.log('file original name：%s', file.originalname);
    console.log('file size：%s', file.size);
    console.log('file path：%s', file.path);

    if (fs.existsSync(file.path)) {

        fs.readdir(config.APK_FOLDER,function(err,files){
            //Delete old apk




            files.forEach(function (oldFile) {
                var oldFilePath = config.APK_FOLDER+ oldFile;

                var i = oldFilePath.indexOf(file.originalname);

                if(i < 0){
                    //Delete all files in the apk folder, to make sure there is only one apk in the folder.
                    //console.log("delete " + config.APK_FOLDER+ oldFile);
                    fs.unlink(config.APK_FOLDER+ oldFile,function(err){
                        if(err) {
                            console.log(err);
                        } else{
                            console.log(' delete ' + oldFile +' successful');
                        }

                    });
                }
            });

        });


        res.send({ret_code: '0',message: 'Upload successful'});
    }else{
        res.send({ret_code:'1' , message: 'Something went wrong, file doesn\'t exist.'})
    }


    // console.log('pic_upload: req.files=' + req.files);

    // if(req.files.apk == null)
    //     return res.json({result:'can not find apk as input'});

});



module.exports = router;
